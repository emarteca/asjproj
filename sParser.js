var fs = require( "fs");
var jstok = require( "js-tokens").default;
var mttok = require( "js-tokens").matchToToken;

class Token {
	constructor( str, typ, lineNum, scope, specScope) {
		this.str = str;
		this.typ = typ;
		this.lineNum = lineNum;
		this.scope = scope;
		this.specScope = specScope;
	}

	getStrRep() {
		return "[ str: " + this.str + ", typ: " + this.typ + ", lineNum: " + this.lineNum + ", scope: " + this.scope + ", specScope: " + this.specScope + "]";
	}
}

function parse( fileName) {
	var fileText = fs.readFileSync( fileName, "utf-8");
	var resOfInitParse = tokenizeAndRemWS( fileText.match( jstok));

	// now, we have the entire text of the code parsed into a list of strings
	// what should we do?
	// maybe, have a function to get a list of variables

	// console.log( resOfInitParse);
	console.log( getListOfVars( resOfInitParse));
	// printTokenArray( resOfInitParse);
	console.log( getLinesWithAwait( resOfInitParse));
}

function printTokenArray( listOfTokens) {
	console.log( "Printing array of tokens: ");
	var strToPrint = "";
	for ( var t of listOfTokens) {
		strToPrint += t.getStrRep() + "\n";

	}
	console.log( strToPrint);
}

function tokenizeAndRemWS( listOfTokens) {

	// need to keep track of scope
	var scopeStack = new Array(); // keep track of the line number of the current stack
	scopeStack.push( 1); // we know scope zero starts at the first line


	// jstokens already combines comments into one thing
	// so, just check the first element of each token, if it's a comment or some white space remove it

	var retList = [];
	var curLine = 1;
	var curScope = 0; // this is the global scope
	for ( var t of listOfTokens) {
		var curLet = t[ 0];
		if ( curLet === " " || curLet === "\n" || curLet === "\t") { // whitespace? 
			if ( curLet === "\n") { // count the number of newlines
				++ curLine;
				var nlc = 1;
				while ( nlc < t.length) {
					if ( t[ nlc] === "\n")
						++ curLine;
					++ nlc;
				}
			}
			continue;
		}

		if ( t.length > 1 && t[ 0] === "/" && ( t[ 1] === "/" || t[ 1] === "*")) { // comment?
			var curComment = t.split( "\n");
			curLine += curComment.length - 1;
			continue;
		}

		if ( curLet === "{") {
			++ curScope;
			scopeStack.push( curLine);
		}
		else if ( curLet === "}") {
			-- curScope;
			scopeStack.pop();
		}

		retList.push( new Token( t, "?", curLine, curScope, scopeStack[ scopeStack.length - 1]));
	}
	return retList;
}

function getListOfVars( listOfTokens) {

	var listOfVars = [];
	for ( var i = 0; i < listOfTokens.length; ++i) {
		
		if ( listOfTokens[ i].str === "var" || listOfTokens[ i].str === "const") { // now we have a variable definition
			// next token should be the name
			listOfVars.push( "Scope: " + listOfTokens[ i].scope + ", specScope: " + listOfTokens[ i].specScope + " -- " + listOfTokens[ i + 1].getStrRep());
		}
	}
	return listOfVars;
}

function getLinesWithAwait( listOfTokens) {
	var retList = [];

	for ( var t of listOfTokens) {
		if ( t.str === "await") {
			retList.push( t.lineNum);
		}
	}
	return retList;
}

function main() {
	if ( process.argv.length != 3) {
		console.log( "Usage: node sParser.js fileToParse");
		return;
	}

	parse( process.argv[ 2]);
}

main();