/**
 * @name testing1
 * @description Insert description here...
 * @kind problem
 * @problem.severity warning
 */

import javascript

// from LoopStmt e
// where not isForAwaitOf(e) 
// select count( ASTNode d | d = e.getBody().getAChild())

//from LoopStmt e
//select DataFlow::valueNode(e.getBody()) //, DataFlow::valueNode(e.getBody()).getAPredecessor()

//from DataFlow::Node n, AwaitExpr ae
//where ae = n.asExpr()
//select n.getBasicBlock()
// select DataFlow::valueNode(e).getAPredecessor()

/*from DataFlow::AnalyzedNode nd, AwaitExpr ae, Scope s
where ae = nd.asExpr() and
	  s = 
select ae*/

// can't use scope, since loops in JS don't constitute a scope

/*from LoopStmt e
where isForAwaitOf(e)
select e.getBody()*/

predicate stmtInLoop(Stmt s) {
	exists (LoopStmt l | l = s.getParentStmt())
	or
	stmtInLoop(s.getParentStmt())
}

predicate exprInLoop(Expr e) {
	exists (LoopStmt l | l = e.getEnclosingStmt())
	or
	stmtInLoop( e.getEnclosingStmt()) 
}

predicate exprOrStmtInLoop(ExprOrStmt es) {
	exists (Stmt s | s = es | exists (LoopStmt l | l = s.getParentStmt())
							  or
							  exprOrStmtInLoop(s.getParentStmt()))
	or
	exists (Expr e | e = es | exists (LoopStmt l | l = e.getEnclosingStmt()) 
						      or 
						      exprOrStmtInLoop( e.getEnclosingStmt()))
}

LoopStmt stmtInLoop_getLoop(Stmt s) {
	result = s.getParentStmt()
	or
	result = stmtInLoop_getLoop(s.getParentStmt())
}

LoopStmt exprInLoop_getLoop(Expr e) {
	result = e.getEnclosingStmt()
	or
	result = stmtInLoop_getLoop( e.getEnclosingStmt()) 
}


//from AwaitExpr e
//select e.getEnclosingStmt(), e.getEnclosingStmt().getParentStmt().getParentStmt().getParentStmt().getParentStmt() //, e.getParentExpr().getParentExpr().getEnclosingStmt()

// get list of all awaits inside loops
/*from AwaitExpr e
where exprInLoop(e)
select e
*/

from AwaitExpr e, LoopStmt l
where l = exprInLoop_getLoop( e) or isForAwaitOf(l)
select l

/*import javascript

// from DataFlow::Node p
// where DataFlow::isIncomplete(p, "await")
// select p.

from DataFlow::Node p
where (exists (AwaitExpr ae | p.asExpr() = ae)) and p.isIncomplete("await")
select p
*/

from AwaitExpr ae, DataFlow::AnalyzedNode an
where an = ae.getAChildExpr().analyze() and an.hasFlow() and ae.analyze().isIncomplete("await")
select ae, ae.getAChildExpr(), ae.getNumChildExpr(), an.getALocalSource()



// for each function, get the list of variables declared in it
// this does not include arguments/parameters
from Variable v, Function f
where exists (FunctionScope fs | v.getScope() = fs and fs.getFunction() = f)
	  and (f.getFile().toString() = "/opt/src/shorter_test.js")
select v, f


//import javascript
//
////from GlobalVarAccess gva
////select gva, gva.getEnclosingFunction(), gva.getFile()
//
//from GlobalVariable gv, TopLevel tl
//where /*(tl.getFile().toString() = "/opt/src/shorter_test.js") and*/ gv.declaredIn(tl)
//select gv, tl.getFile().toString()
