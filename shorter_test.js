const fetch = require('node-fetch');
const { PerformanceObserver, performance } = require('perf_hooks');

// function to process csv and turn it into an array of arrays of strings
function process( dataString) {
  var lines = dataString.split(/\n/).map(function( lineStr) {
        return lineStr.split(",");   
    }).slice(1);                     // get rid of first line since it's just the headers
  
  return lines;
}

// function to get the list of URLs from a specified page which contains URLs
// returns an array of URLs
async function getURLs( urlToFetch) {
	return ( process( await (await fetch(urlToFetch)).text())).map( ar => ar[ 0]); // first element in array is the URL we want	    	
}

// function to count the number of occurrences of a particular string
function getNumOccurrences( strArray, strToCompare) {
	return strArray.filter( v => v == strToCompare).length;
}

async function fetchAndResolve( urlToFetch, strName) {
	try {
		var contents = await (await fetch( urlToFetch)).text();
		console.log( strName + " number of occurrences of \"and\": " + getNumOccurrences( contents.split(/[ \n\t]/), "and"));
	} catch( e) {
		console.log( "Error at " + strName);
	}
}



async function testFetch_nonoverSynch_mapLoop( urlToFetch) {
	var t0 = performance.now();

	var urlList = await getURLs( urlToFetch);

	var pList = urlList.map( ( curUrl, curInd) => fetchAndResolve( curUrl, "URL: " + curInd));

	var pAll = Promise.all( pList);
	var t1;
	pAll.then( function( v) {
		t1 = performance.now();
		console.log("Call to testFetch_nonoverSynch_mapLoop took " + (t1 - t0) + " milliseconds.");
	});
	return ( t1 - t0);
}

async function testFetch_overSynch_awaitInFor( urlToFetch) {
	var t0 = performance.now();

	var urlList = await getURLs( urlToFetch);

	pList = []

	// oversynchronized await version
	for ( var i = 0; i < urlList.length; ++i) {
		var k = undefined;
		try { 
			k = await fetchAndResolve( urlList[ i], "URL: " + i);
		} catch( e) { }
		pList.push( k);
	}

	// should be oversynch but just in case
	var t1 = performance.now();
	console.log("Call to testFetch_overSynch_awaitInFor took " + (t1 - t0) + " milliseconds.");
	return ( t1 - t0);
}

async function testFetch_overSynch_forawait( urlToFetch) {
	var t0 = performance.now();

	var urlList = await getURLs( urlToFetch);

	pList = []

	// oversynchronized await version
	var i = 0;
	for await ( var curUrl of urlList) {
		pList.push( fetchAndResolve( urlList[ i], "URL: " + i));
		++ i;
	}

	// should be oversynch but just in case
	var pAll = Promise.all( pList);
	var t1;
	pAll.then( function( v) {
		t1 = performance.now();
		console.log("Call to testFetch_overSynch_forawait took " + (t1 - t0) + " milliseconds.");
	});
	return ( t1 - t0);
}

testFetch_nonoverSynch_mapLoop( 'https://cdn.rawgit.com/citizenlab/test-lists/04811299/lists/ae.csv')