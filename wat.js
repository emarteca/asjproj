function resolveAfter(resolveFun, value, msec){
	 setTimeout(function(){ resolveFun(value); }, msec);
}
var p1 = new Promise(function(resolve,reject){ resolveAfter(resolve, 3, 1000)});
var p2 = new Promise(function(resolve,reject){ resolveAfter(resolve, 5, 500)});
var p3 = new Promise(function(resolve,reject){ resolveAfter(resolve, 7, 2000)});
var p4 = Promise.all([p1, p2, p3]);
p4.then(function(v){
	 console.log(v); // prints [3, 5, 7] after >= 2000 msec
}); 
