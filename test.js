const fetch = require('node-fetch');
const { PerformanceObserver, performance } = require('perf_hooks');

// function to process csv and turn it into an array of arrays of strings
function process( dataString) {
  var lines = dataString.split(/\n/).map(function( lineStr) {
        return lineStr.split(",");   
    }).slice(1);                     // get rid of first line since it's just the headers
  
  return lines;
}

// function to get the list of URLs from a specified page which contains URLs
// returns an array of URLs
async function getURLs( urlToFetch) {
	// was testing with the following url:
	// https://github.com/citizenlab/test-lists/blob/master/lists/ae.csv
	// var urlToFetch = 'https://cdn.rawgit.com/citizenlab/test-lists/04811299/lists/ae.csv';
	
	var output;
	
	await fetch(urlToFetch)
		.then(response => response.text())
		.then( function( data) {
				output = ( process( data)).map( ar => ar[ 0]); // first element in array is the URL we want
		    	// console.log( output); 
			}
		)
		.catch(error => console.error(error));

	return output;
}

// function to get the most frequent word in an array of strings
function getModeWord( strArray) {
	if ( strArray.length == 0)
		return undefined;

	strArray.sort(); // array is sorted now
	var curWord = strArray[ 0];
	var modeWord = strArray[ 0];
	var curOccurences = 0;
	var modeOccurences = 0;
	for ( var v of strArray) {
		// check if the current string is the current word we're looking at
		// if it is, increase the counter
		if ( v === curWord) {
			curOccurences ++;
		} else {
			// if it's not, we know we're onto the next word. 
			// check to see if we need to update the mode word, and if not, reset counter for curWord
			// this works fine on the first iteration of the loop, since curWord and modeWord both start being the first element
			// but the counts are both zero
			if ( curOccurences > modeOccurences) {
				modeOccurences = curOccurences;
				modeWord = curWord;
			}
			curWord = v;
			curOccurences = 1; // we only started at zero outside the loop bc we incremented in the first iteration of the loop, i.e. for strArray[ 0]
		}
	}

	// check again once outside in case it was the last word
	if ( curOccurences > modeOccurences) {
		modeOccurences = curOccurences;
		modeWord = curWord;
	}
	return modeWord;
} 

// function to fetch from the specified URL, and print the length of the 
// result string to the console
// also prints the URL id (i.e. its spot in the URL list), so we can see 
// in which order the list is being processed
// counts the number of occurrences of the word substring specified
/*async function fetchAndResolve( urlToFetch, strName, resolveFun, rejectFun) {
	fetch( urlToFetch).then(
		res => res.text()).then(
			function( body) {
				var modeWord = getModeWord( body.split(/[ \n\t]/));
				if ( modeWord === "")
					console.log( strName + " N/A");
				else
					console.log( strName + " mode word was: "  + modeWord);
			})
			.then( 
				function( v) { resolveFun( 1);
			}).catch( function( e) {
				console.log( "Error at " + strName);
				rejectFun( 3);
			});
}*/
async function fetchAndResolve( urlToFetch, strName) {
	return fetch( urlToFetch).then(
				res => res.text()).then(
					function( body) {
						var modeWord = getModeWord( body.split(/[ \n\t]/));
						if ( modeWord === "")
							console.log( strName + " N/A");
						else
							console.log( strName + " mode word was: "  + modeWord);
					})
					// .then( 
					// 	function( v) { resolveFun( 1);
					// })
					.catch( function( e) {
						console.log( "Error at " + strName);
						//rejectFun( 3);
					});
}


// function which actually runs the fetches
// this version runs without oversynchronization 
async function testFetch_nonOverSynch( urlToFetch) {
	var t0 = performance.now();

	var urlList = await getURLs( urlToFetch);

	pList = [];

	// regular promise version
	for ( var i = 0; i < urlList.length; ++i) {
		pList.push( fetchAndResolve( urlList[ i], "1 URL: " + i));
	}

	var pAll = Promise.all( pList);
	var t1;
	await pAll.then( function( v) {
		console.log( "Done 1!");
		t1 = performance.now();
		console.log("1 -- Call to testFetch_nonOverSynch took " + (t1 - t0) + " milliseconds.");
	});
	return ( t1 - t0);
}

async function testFetch_nonoverSynch_mapLoop( urlToFetch) {
	var t0 = performance.now();

	var urlList = await getURLs( urlToFetch);

	var pList = urlList.map( ( curUrl, curInd) => fetchAndResolve( curUrl, "2 URL: " + curInd));

	var pAll = Promise.all( pList);
	var t1;
	await pAll.then( function( v) {
		console.log( "Done 2!");
		t1 = performance.now();
		console.log("2 -- Call to testFetch_nonoverSynch_mapLoop took " + (t1 - t0) + " milliseconds.");
	});
	return ( t1 - t0);
}

// function which actually runs the fetches
// this version runs with oversynchronization 
async function testFetch_overSynch_forawait( urlToFetch) {
	var t0 = performance.now();

	var urlList = await getURLs( urlToFetch);

	pList = []

	// oversynchronized await version
	var i = 0;
	for await ( var curUrl of urlList) {
		pList.push( fetchAndResolve( urlList[ i], "3 URL: " + i));
		++ i;
	}

	// should be oversynch but just in case
	var pAll = Promise.all( pList);
	var t1;
	await pAll.then( function( v) {
		console.log( "Done 3!");
		t1 = performance.now();
		console.log("3 -- Call to testFetch_overSynch_forawait took " + (t1 - t0) + " milliseconds.");
	});
	return ( t1 - t0);
}

async function testFetch_overSynch_awaitInFor( urlToFetch) {
	var t0 = performance.now();

	var urlList = await getURLs( urlToFetch);

	pList = []

	// oversynchronized await version
	for ( var i = 0; i < urlList.length; ++i) {
		var k = undefined;
		try { 
			k = await fetchAndResolve( urlList[ i], "4 URL: " + i);
		} catch( e) { }
		pList.push( k);
	}

	// should be oversynch but just in case
	var pAll = Promise.all( pList);
	var t1;
	await pAll.then( function( v) {
		console.log( "Done 4!");
		t1 = performance.now();
		console.log("4 -- Call to testFetch_overSynch_awaitInFor took " + (t1 - t0) + " milliseconds.");
	});
	return ( t1 - t0);
}

async function testFetch_overSynch_mapLoop( urlToFetch) {
	var t0 = performance.now();
	
	var urlList = await getURLs( urlToFetch);

	pList = []

	for await ( var p of urlList.map( ( curUrl, curInd) => fetchAndResolve( curUrl, "5 URL: " + curInd))) {
		pList.push( p);
	}

	// should be oversynch but just in case
	var pAll = Promise.all( pList);
	var t1;
	await pAll.then( function( v) {
		console.log( "Done 5!");
		var t1 = performance.now();
		console.log("5 -- Call to testFetch_overSynch_mapLoop took " + (t1 - t0) + " milliseconds.");
	});
	return ( t1 - t0);
}

async function testAll() {
	var t = 0;

	t = testFetch_nonOverSynch( 'https://cdn.rawgit.com/citizenlab/test-lists/04811299/lists/ae.csv');
	await t.then( v => console.log( "Call to testFetch_nonOverSynch took " + v + " milliseconds."))
		.catch( e => console.log( "Error in testFetch_nonOverSynch"));
	
	t = testFetch_nonoverSynch_mapLoop( 'https://cdn.rawgit.com/citizenlab/test-lists/04811299/lists/ae.csv');
	await t.then( v => console.log( "Call to testFetch_nonoverSynch_mapLoop took " + v + " milliseconds."))
		.catch( e => console.log( "Error in testFetch_nonoverSynch_mapLoop"));
	
	t = testFetch_overSynch_forawait( 'https://cdn.rawgit.com/citizenlab/test-lists/04811299/lists/ae.csv');
	await t.then( v => console.log( "Call to testFetch_overSynch_forawait took " + v + " milliseconds."))
		.catch( e => console.log( "Error in testFetch_overSynch_forawait"));
	
	t = testFetch_overSynch_awaitInFor( 'https://cdn.rawgit.com/citizenlab/test-lists/04811299/lists/ae.csv');
	await t.then( v => console.log( "Call to testFetch_overSynch_awaitInFor took " + v + " milliseconds."))
		.catch( e => console.log("Error in testFetch_overSynch_awaitInFor"));
	
	t = testFetch_overSynch_mapLoop( 'https://cdn.rawgit.com/citizenlab/test-lists/04811299/lists/ae.csv');
	await t.then( v => console.log( "Call to testFetch_overSynch_mapLoop took " + v + " milliseconds."))
		.catch( e => console.log( "Error in testFetch_overSynch_mapLoop"));
}

testAll();

// testFetch_nonOverSynch   -- 1 -- Call to testFetch_nonOverSynch took 163356.68626499176 milliseconds.
// testFetch_nonoverSynch_mapLoop -- 2 -- Call to testFetch_nonoverSynch_mapLoop took 148478.79533803463 milliseconds.
// testFetch_overSynch_forawait -- 3 -- Call to testFetch_overSynch_forawait took 155305.65076601505 milliseconds.
// testFetch_overSynch_awaitInFor 4 -- Call to testFetch_overSynch_awaitInFor took 1383425.4624569416 milliseconds.
// testFetch_overSynch_mapLoop -- 5 -- Call to testFetch_overSynch_mapLoop took 279922.63767403364 milliseconds.









// old output

// nonoversynch: real 2m43.394s
//				 user 0m9.920s
//				 sys  0m1.011s

// oversynch: real 22m3.692s
//			  user 0m13.586s
//			  sys 0m1.770s
